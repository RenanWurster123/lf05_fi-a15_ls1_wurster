package renan.array;
import java.util.Scanner;
public class ArrayAufagaben {

    public static void main(String[] args) {
        System.out.println("Aufgabe 1:");
        zahlen();
        System.out.println("Aufgabe 2:");
        ungeradezahlen();
        System.out.println("Aufgabe 3:");
        palindrom();
        System.out.println("Aufgabe 4:");
        lotto();
    } // end of main


    public static void zahlen(){
        int[] zahlenfeld = new int[10];
        for (int i = 0; i < zahlenfeld.length; i++) {
            zahlenfeld[i] = i;
        }
        for (int i = 0; i < zahlenfeld.length; i++) {
            System.out.print(zahlenfeld[i] + " ");
        }
        System.out.println();
    }

    public static void ungeradezahlen(){
        int[] zahlenfeld = new int[10];
        int x = 1;
        for (int i = 0; i < zahlenfeld.length; i++) {
            zahlenfeld[i] = x;
            x+=2;
        }
        for (int i = 0; i < zahlenfeld.length; i++) {
            System.out.print(zahlenfeld[i] + " ");
        }
        System.out.println();
    }

    public static void palindrom(){
        int[] zeichen = new int[5];
        Scanner sc = new Scanner(System.in);
        for (int i = 0; i < zeichen.length; i++) {
            System.out.println("Bitte geben Sie die " + (i+1) + ". Zahl ein:");
            zeichen[i] = sc.nextInt();
        }
        System.out.println("Die Zahlen sind in umgekehrter Reihenfolge:");
        for (int i = zeichen.length-1; i >= 0; i--) {
            System.out.print(zeichen[i] + " ");
        }
        System.out.println();
    }

    public static void lotto(){
        int[] lottozahlen = {3, 7, 12, 18, 37, 42};
        boolean enthalten12 = false;
        boolean enthalten13 = false;
        System.out.print("[ ");
        for (int i = 0; i < lottozahlen.length; i++) {
            System.out.print(lottozahlen[i] + " ");
        }
        System.out.println("]");
        for (int i = 0; i < lottozahlen.length; i++) {
            if(lottozahlen[i] == 12){
                enthalten12 = true;
                break;
            }
        }
        for (int i = 0; i < lottozahlen.length; i++) {
            if(lottozahlen[i] == 13){
                enthalten13 = true;
                break;
            }
        }
        if(enthalten12) System.out.println("Die 12 ist enthalten.");
        else System.out.println("Die 12 ist nicht enthalten.");
        if(enthalten13) System.out.println("Die 13 ist enthalten.");
        else System.out.println("Die 13 ist nicht enthalten.");
        System.out.println();
    }
}
