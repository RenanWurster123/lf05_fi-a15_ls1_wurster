package renan.bmi;

import java.util.Scanner;

public class Bmi {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Was ist Ihre Körpergewicht?");
        float weight = scanner.nextFloat();
        System.out.println("Was ist Ihre Körpergröße?");
        float height = scanner.nextFloat();
        System.out.println("Sind Sie W oder M? W für Frau und M für Männer");
        boolean result = scanner.nextBoolean();


        float bmi = (100*100*weight)/(height*height);
        System.out.println("My Body Mass Index is: " + bmi);

        if (bmi < 18.5) {
            System.out.println("I'm underweight");
        } else if (bmi >= 18.5 && bmi < 25) {
            System.out.println("I'm healthy");
        } else if (bmi >= 25 && bmi < 30) {
            System.out.println("I'm overweight");
        } else if (bmi >= 30 && bmi < 35) {
            System.out.println("I'm obese 1");
        } else if (bmi >= 35 && bmi < 40) {
            System.out.println("I'm obese 2");
        } else if (bmi >= 40) {
            System.out.println("I'm obese 3");
        }


    }
}
