package renan.weltderzahlen;

public class WeltDerZahlen {

    public static void main(String[] args) {

    /*  *********************************************************

         Zuerst werden die Variablen mit den Werten festgelegt!

    *********************************************************** */
        // Im Internet gefunden ?
        // Die Anzahl der Planeten in unserem Sonnesystem
        int anzahlPlaneten = 8;

        // Anzahl der Sterne in unserer Milchstraße

        long anzahlSterne = 200000000;

        // Wie viele Einwohner hat Berlin?
        int bewohnerBerlin = 3566791;

        // Wie alt bist du?  Wie viele Tage sind das?

        int alterTage = 11680;

        // Wie viel wiegt das schwerste Tier der Welt?
        // Schreiben Sie das Gewicht in Kilogramm auf!
        int gewichtKilogramm = 190000;

        // Schreiben Sie auf, wie viele km² das größte Land der Erde hat?
        int flaecheGroessteLand = 17000000;

        // Wie groß ist das kleinste Land der Erde?

        byte flaecheKleinsteLand = 1;




    /*  *********************************************************

         Programmieren Sie jetzt die Ausgaben der Werte in den Variablen

    *********************************************************** */

        System.out.println("Anzhahl der Planeten: " + anzahlPlaneten);

        System.out.println("Anzahl der Sterne: " + anzahlSterne);

        System.out.println("Eiwohner in Berlin: " + bewohnerBerlin);

        System.out.println("Mein Alter in Tagen: " + alterTage);

        System.out.println("Gewicht des schwersten Tiers der Welt: " + gewichtKilogramm + "Kg");

        System.out.println("Größte Land der Erde: " + flaecheGroessteLand + "km²");

        System.out.println("Kleinste Land der Erde: " + flaecheKleinsteLand + "km²");


        System.out.println(" *******  Ende des Programms  *******  ");

    }
}