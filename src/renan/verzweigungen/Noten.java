package renan.verzweigungen;

import java.util.Scanner;

public class Noten {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Gib bitte ein Ziffernnoten");
        int ziffern = scanner.nextInt();
        if (ziffern == 1) {
            System.out.println("Sehr gut");
        } else if (ziffern == 2) {
            System.out.println("Gut");
        } else if (ziffern == 3) {
            System.out.println("Befriedigend");
        } else if (ziffern == 4) {
            System.out.println("Ausreichend");
        } else if (ziffern == 5) {
            System.out.println("Mangelhaft");
        } else if (ziffern == 6) {
            System.out.println("Ungenügend");
        } else {
            System.out.println("Ziffer nicht erlaubt");
        }
    }
}
