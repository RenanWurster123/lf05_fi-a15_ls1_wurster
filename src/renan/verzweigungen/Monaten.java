package renan.verzweigungen;

import java.util.Scanner;

public class Monaten {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Gib bitte ein Ziffer für die entsprechende Monate");
        int ziffern = scanner.nextInt();
        if (ziffern == 1) {
            System.out.println("Januar");
        } else if (ziffern == 2) {
            System.out.println("Februar");
        } else if (ziffern == 3) {
            System.out.println("März");
        } else if (ziffern == 4) {
            System.out.println("April");
        } else if (ziffern == 5) {
            System.out.println("Mai");
        } else if (ziffern == 6) {
            System.out.println("Juni");
        }else if (ziffern == 7) {
            System.out.println("Juli");
        }else if (ziffern == 8) {
            System.out.println("August");
        }else if (ziffern == 9) {
            System.out.println("September");
        }else if (ziffern == 10) {
            System.out.println("Oktober");
        }else if (ziffern == 11) {
            System.out.println("November");
        }else if (ziffern == 12) {
            System.out.println("Dezember");
        } else {
            System.out.println("Ziffer nicht erlaubt");
        }
    }
}

