package renan.fibonacci;

public class Fibonacci {

        public static void main(String[] args) {

            int a = 0;
            int b = 1;

            for (int i = 0; i < 12; i++) {

                a = b-a;
                b = a+b;
                System.out.println("Fibonacci: " + a);
            }
        }

}
