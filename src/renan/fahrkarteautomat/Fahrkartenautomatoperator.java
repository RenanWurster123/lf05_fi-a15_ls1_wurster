package renan.fahrkarteautomat;

import java.util.Scanner; // Import der Klasse Scanner
class FahrkartenautomatOperator
{

    public static void main(String[] args) // Hier startet das Programm
    {

        Scanner scanner = new Scanner(System.in);

        System.out.print("Bitte geben Sie eine ganze Zahl ein: ");
        int zahl1 = scanner.nextInt();

        System.out.print("Gib einen Rechenoperator ein ( + | - | * | / ): ");
        String operator = scanner.next();

        System.out.print("Bitte geben Sie eine zweite ganze Zahl ein: ");
        int zahl2 = scanner.nextInt();

        scanner.close();

        int ergebnis = switch (operator) {
            case "+" -> zahl1 + zahl2;
            case "-" -> zahl1 - zahl2;
            case "*" -> zahl1 * zahl2;
            case "/" -> zahl1 / zahl2;
            default -> -1;
        };

        System.out.print("\n\nErgebnis der Addition lautet: ");
        System.out.print(zahl1 + " " + operator + " " + zahl2 + " = " + ergebnis);





    }
}