package renan.fahrkarteautomat;

import java.util.Scanner;

public class Fahkartenkombinieren {
    public static double fahrkartenbestellungErfassen() {
        int ticket;
        int anzahlTickets;
        double ticketPreis = 0;
        double gesamtPreis = 0;
        Scanner tastatur = new Scanner(System.in);

        do {

            do {
                System.out.println("Wählen Sie Ihre gewünschte Fahrkarte für Berlin AB aus:");
                System.out.printf("%3s\n", "Einzelfahrschein Regeltarif AB [3,00 Euro] (1)");
                System.out.printf("%3s\n", "4-Fahrten-Karte Einzelfahrausweis AB [9,40 Euro] (2)");
                System.out.printf("%3s\n", "24-Stunden-Karte Regeltarif AB [8,80 Euro] (3)");
                System.out.printf("%3s\n\n", "Bezahlen (9)");

                System.out.print("Ihre Wahl: ");
                ticket = tastatur.nextInt();
                if (ticket < 1 || ticket > 3) {
                    if (ticket == 9 & gesamtPreis != 0) {
                        break;
                    }
                    System.out.println("Ungültige Eingabe!\n");
                }
            } while (ticket < 1 || ticket > 3);

            if (ticket == 1) {
                ticketPreis = 3.00;
            } else if (ticket == 2) {
                ticketPreis = 9.40;
            } else if (ticket == 3){
                ticketPreis = 8.80;
            } else if (ticket == 9) {
                break;
            }

            do {
                System.out.print("Anzahl der Tickets (min: 1 | max: 10): ");
                anzahlTickets = tastatur.nextInt();

                if (anzahlTickets < 1 || anzahlTickets > 10) {
                    System.out.printf("\nDie Eingabe für die Anzahl der Tickets ist ungültig (%d).\nDer Wert muss zwischen 1-10 liegen. \nBitte erneut eingeben.\n\n", anzahlTickets);
                    anzahlTickets = 1;
                } else {
                    break;
                }
            } while (true);

            gesamtPreis += ticketPreis * anzahlTickets;
            System.out.printf("\nZwischensumme: %.2f\n\n", gesamtPreis);

        } while (true);

        return gesamtPreis;
    }

    public static double fahrkartenBezahlen(double zuZahlenderBetrag) {
        double eingeworfenemuenze;
        double eingezahlterGesamtbetrag = 0.0;
        Scanner tastatur = new Scanner(System.in);

        while (eingezahlterGesamtbetrag < zuZahlenderBetrag) {
            System.out.format("\nNoch zu zahlen: %4.2f Euro %n", (zuZahlenderBetrag - eingezahlterGesamtbetrag));
            System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
            eingeworfenemuenze = tastatur.nextDouble();
            eingezahlterGesamtbetrag += eingeworfenemuenze;
        }
        return eingezahlterGesamtbetrag - zuZahlenderBetrag;
    }

    public static void fahrkartenAusgeben() {
        System.out.println("\nFahrschein wird ausgegeben");
        for (int i = 0; i < 8; i++) {
            System.out.print("=");
            try {
                Thread.sleep(250);
            } catch (InterruptedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        System.out.println("\n\n");
    }

    public static void rueckgeldAusgeben(double rueckgabebetrag) {

        if (rueckgabebetrag > 0.0) {
            System.out.format("Der Rückgabebetrag in Höhe von %4.2f Euro %n", rueckgabebetrag);
            System.out.println("wird in folgenden Münzen ausgezahlt:");

            while (rueckgabebetrag >= 2.0) {// 2 EURO-muenzen
                System.out.println("2 EURO");
                rueckgabebetrag -= 2.0;
            }
            while (rueckgabebetrag >= 1.0) {// 1 EURO-muenzen
                System.out.println("1 EURO");
                rueckgabebetrag -= 1.0;
            }
            while (rueckgabebetrag >= 0.5) // 50 CENT-muenzen
            {
                System.out.println("50 CENT");
                rueckgabebetrag -= 0.5;
            }
            while (rueckgabebetrag >= 0.2) // 20 CENT-muenzen
            {
                System.out.println("20 CENT");
                rueckgabebetrag -= 0.2;
            }
            while (rueckgabebetrag >= 0.1) // 10 CENT-MÃ¼zen
            {
                System.out.println("10 CENT");
                rueckgabebetrag -= 0.1;
            }
            while (rueckgabebetrag >= 0.05)// 5 CENT-muenzen
            {
                System.out.println("5 CENT");
                rueckgabebetrag -= 0.05;
            }
        }
    }

    public static void main(String[] args) {

        while (true) {
            double zuZahlenderBetrag;
            double rueckgabebetrag;

            zuZahlenderBetrag = fahrkartenbestellungErfassen();
            rueckgabebetrag = fahrkartenBezahlen(zuZahlenderBetrag);
            fahrkartenAusgeben();
            rueckgeldAusgeben(rueckgabebetrag);

            System.out.println("\nVergessen Sie nicht, den Fahrschein\n" + "vor Fahrtantritt entwerten zu lassen!\n"
                    + "Wir wünschen Ihnen eine gute Fahrt.");
            System.out.print("\n\n\n");
        }
    }
}
