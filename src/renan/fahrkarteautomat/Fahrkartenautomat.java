package renan.fahrkarteautomat;

import java.util.Scanner;

class Fahrkartenautomat {


    //Vorteile: Daher sind Arrays im Vergleich zu verknüpften Listen und Hash-Tabellen effizienter und vorteilhafter.
    // Sie sind schneller und können überall eingesetzt werden. Sie speichern Daten ähnlicher Datentypen zusammen und können überall im Code verwendet werden.
    // Daher sind sie bei der Speicherzuweisung effizienter und sollten in allen modernen Sprachen verwendet werden.

    //Nachteile: Es erlaubt uns, nur eine feste Anzahl von Elementen einzugeben. Wir können die Größe des Arrays nicht ändern, sobald das Array deklariert ist.
    // Wenn wir also mehr Datensätze einfügen müssen als deklariert, ist dies nicht möglich.
    public static double fahrkartenbestellungErfassen() {
        int anzahlTickets;
        double ticketPreis;
        Scanner tastatur = new Scanner(System.in);
        int auswahl;
        String[] bezeichnung = new String[]{"Einzelfahrschein Berlin AB", "Einzelfahrschein Berlin BC", "Einzelfahrschein Berlin ABC", "Kurzstrecke", "Tageskarte Berlin AB", "Tageskarte Berlin BC", "Tageskarte Berlin ABC", "Kleingruppen-Tageskarte Berlin AB", "Kleingruppen-Tageskarte Berlin BC", "Kleingruppen-Tageskarte Berlin ABC"};
        double[] preis = new double[]{2.90, 3.30, 3.60, 1.90, 8.60, 9.00, 9.60, 23.50, 24.30, 24.90};


        System.out.println("Fahrkartenbestellvorgang:");
        System.out.println("===========================\n");
        System.out.println("Waehlen Sie ihre Wunschfahrkarte für Berlin AB aus:");
        for (int i = 0; i < bezeichnung.length; i++) {
            System.out.println(bezeichnung[i] + " - Preis: " + preis[i] + " Fahrkartenummer: " + i);
        }

            System.out.print("Ihre Wahl:");
            auswahl = tastatur.nextInt();

        while (auswahl < 0 || auswahl > 9) {
            System.out.print(" >>falsche Eingabe<<\n " + "Ihre Wahl:");
            auswahl = tastatur.nextInt();
        }

        System.out.print("Anzahl der Tickets: ");
        anzahlTickets = tastatur.nextInt();

        while (anzahlTickets < 1 || anzahlTickets >= 11) {
            System.out.print("Wählen Sie bitte eine Anzahl von 1 bis 10 Tickets aus.\n " + "Anzahl der Tickets: ");
            anzahlTickets = tastatur.nextInt();
        }
        ticketPreis = preis[auswahl];


        return ticketPreis * anzahlTickets;

    }

    public static double fahrkartenBezahlen(double zuZahlenderBetrag) {
        double eingeworfenemuenze;
        double eingezahlterGesamtbetrag = 0.0;
        Scanner tastatur = new Scanner(System.in);

        while (eingezahlterGesamtbetrag < zuZahlenderBetrag) {
            System.out.format("Noch zu zahlen: %4.2f € %n", (zuZahlenderBetrag - eingezahlterGesamtbetrag));
            System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
            eingeworfenemuenze = tastatur.nextDouble();
            eingezahlterGesamtbetrag += eingeworfenemuenze;
        }
        return eingezahlterGesamtbetrag - zuZahlenderBetrag;
    }

    public static void fahrkartenAusgeben() {
        System.out.println("\nFahrschein wird ausgegeben");
        for (int i = 0; i < 8; i++) {
            System.out.print("=");
            try {
                Thread.sleep(250);
            } catch (InterruptedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        System.out.println("\n\n");
    }

    public static void rueckgeldAusgeben(double rueckgabebetrag) {

        if (rueckgabebetrag > 0.0) {
            System.out.format("Der Rückgabebetrag in Höhe von %4.2f € %n", rueckgabebetrag);
            System.out.println("wird in folgenden Münzen ausgezahlt:");

            while (rueckgabebetrag >= 2.0) {// 2 EURO-muenzen
                System.out.println("2 EURO");
                rueckgabebetrag -= 2.0;
            }
            while (rueckgabebetrag >= 1.0) {// 1 EURO-muenzen
                System.out.println("1 EURO");
                rueckgabebetrag -= 1.0;
            }
            while (rueckgabebetrag >= 0.5) // 50 CENT-muenzen
            {
                System.out.println("50 CENT");
                rueckgabebetrag -= 0.5;
            }
            while (rueckgabebetrag >= 0.2) // 20 CENT-muenzen
            {
                System.out.println("20 CENT");
                rueckgabebetrag -= 0.2;
            }
            while (rueckgabebetrag >= 0.1) // 10 CENT-Müzen
            {
                System.out.println("10 CENT");
                rueckgabebetrag -= 0.1;
            }
            while (rueckgabebetrag >= 0.05)// 5 CENT-muenzen
            {
                System.out.println("5 CENT");
                rueckgabebetrag -= 0.05;
            }
        }
    }

    public static void main(String[] args) {

        double zuZahlenderBetrag;
        double rueckgabebetrag;

        zuZahlenderBetrag = fahrkartenbestellungErfassen();
        rueckgabebetrag = fahrkartenBezahlen(zuZahlenderBetrag);
        fahrkartenAusgeben();
        rueckgeldAusgeben(rueckgabebetrag);

        System.out.println("\nVergessen Sie nicht, den Fahrschein\n" + "vor Fahrtantritt entwerten zu lassen!\n"
                + "Wir wünschen Ihnen eine gute Fahrt.");
    }
}