package renan.fahrkarteautomat;

import java.util.Scanner;

public class Train {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int fahrzeit = 0;
        char haltInSpandau = 'n';
        char richtungHamburg = 'n';
        char haltInStendal = 'j';
        char endetIn = 'h';

        fahrzeit = fahrzeit + 8; // Fahrzeit: Berlin Hbf -> Spandau

        System.out.println("Soll der Zug in Spandau halten?");
        haltInSpandau = scanner.next().charAt(0);

        if (haltInSpandau == 'j') {
            fahrzeit = fahrzeit + 2; // Halt in Spandau
        } 

        System.out.println("Wollen Sie in die Fahrrichtung Hamburg fahren?");
        richtungHamburg = scanner.next().charAt(0);

        if (richtungHamburg == 'j') {
            fahrzeit += 96;
            System.out.println("Sie erreichen Hamburg nach " + fahrzeit + " Minuten");
        } else {
            fahrzeit += 34;

            System.out.println("Wollen Sie durch Stendal fahren?");
            haltInStendal = scanner.next().charAt(0);

            if (haltInStendal == 'j') {
                fahrzeit += 16;
            } else {
                fahrzeit += 6;
            }

            System.out.println("Wo mochten Sie stoppen?");
            endetIn = scanner.next().charAt(0);

            if (endetIn == 'w') {
                fahrzeit += 29;
                System.out.println("Sie erreichen Wolfsburg nach " + fahrzeit + " Minuten");
            } else if (endetIn == 'b') {
                fahrzeit += 50;
                System.out.println("Sie erreichen Braunschweig nach " + fahrzeit + " Minuten");
            } else {
                fahrzeit += 62;
                System.out.println("Sie erreichen Hannover nach " + fahrzeit + " Minuten");
            }
        }

    }

}