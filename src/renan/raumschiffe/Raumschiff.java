package renan.raumschiffe;

import java.util.ArrayList;
import java.util.List;

public class Raumschiff {
    private int photonentorpedoAnzahl;
    private int energieversorgungInProzent;
    private int schildeInProzent;
    private int huelleInProzent;
    private int lebenserhaltungssystemeInProzent;
    private int androidenAnzahl;
    private String schiffsname;
    ArrayList<Ladung> ladungsverzeichnis = new ArrayList<>();

    public Raumschiff() {

    }

    public Raumschiff(
            int photonentorpedoAnzahl,
            int energieversorgungInProzent,
            int zustandSchildeInProzent,
            int zustandHuelleInProzent,
            int zustandLebenserhaltungssystemeInProzent,
            int anzahlDroiden,
            String schiffsname
    ) {
        setPhotonentorpedoAnzahl(photonentorpedoAnzahl);
        setEnergieversorgungInProzent(energieversorgungInProzent);
        setSchildeInProzent(zustandSchildeInProzent);
        setHuelleInProzent(zustandHuelleInProzent);
        setLebenserhaltungssystemeInProzent(zustandLebenserhaltungssystemeInProzent);
        setAndroidenAnzahl(anzahlDroiden);
        setSchiffsname(schiffsname);
    }


    public int getPhotonentorpedoAnzahl() {
        return this.photonentorpedoAnzahl;
    }

    public void setPhotonentorpedoAnzahl(int photonentorpedoAnzahl) {
        this.photonentorpedoAnzahl = photonentorpedoAnzahl;
    }

    public int getEnergieversorgungInProzent() {
        return this.energieversorgungInProzent;
    }

    public void setEnergieversorgungInProzent(int energieversorgungInProzent) {
        this.energieversorgungInProzent = energieversorgungInProzent;
    }

    public int getSchildeInProzent() {
        return this.schildeInProzent;
    }

    public void setSchildeInProzent(int schildeInProzent) {
        this.schildeInProzent = schildeInProzent;
    }

    public int getHuelleInProzent() {
        return this.huelleInProzent;
    }

    public void setHuelleInProzent(int huelleInProzent) {
        this.huelleInProzent = huelleInProzent;
    }

    public int getLebenserhaltungssystemeInProzent() {
        return this.lebenserhaltungssystemeInProzent;
    }

    public void setLebenserhaltungssystemeInProzent(int lebenserhaltungssystemeInProzent) {
        this.lebenserhaltungssystemeInProzent = lebenserhaltungssystemeInProzent;
    }

    public int getAndroidenAnzahl() {
        return this.androidenAnzahl;
    }

    public void setAndroidenAnzahl(int androidenAnzahl) {
        this.androidenAnzahl = androidenAnzahl;
    }

    public String getSchiffsname() {
        return this.schiffsname;
    }

    public void setSchiffsname(String schiffsname) {
        this.schiffsname = schiffsname;
    }

    public void addLadung(Ladung... ladungen) {
        ladungsverzeichnis.addAll(List.of(ladungen));
    }

    public void photonentorpedoSchiessen(Raumschiff r) {

    }

    public void phaserkanoneSchiessen(Raumschiff r) {

    }

    private void treffer(Raumschiff r) {

    }

    public void nachrichtAnAlle(String message) {

    }

    public void zustandRaumschiff() {

    }

    public void ladungsverzeichnisAusgeben() {

    }
}


