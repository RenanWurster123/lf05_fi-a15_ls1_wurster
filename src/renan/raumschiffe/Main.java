package renan.raumschiffe;


public class Main {
    public static void main(String[] args) {

        Raumschiff klingonen = new Raumschiff(
                1,
                100,
                100,
                100,
                100,
                2,
                "IKS Heghta"
        );

        Ladung klingonenLadung1 = new Ladung("Ferengi Schnecksaft", 200);
        Ladung klingonenladung2 = new Ladung("Bat'leth Klingonen Schwert", 200);

        klingonen.addLadung(klingonenLadung1, klingonenladung2);

        Raumschiff romulaner = new Raumschiff(
                2,
                100,
                100,
                100,
                100,
                2,
                "IRW Khazara"
        );

        Ladung romulanerLadung1 = new Ladung("Borg-Schrott", 5);
        Ladung romulanerLadung2 = new Ladung("Rote Materia", 2);
        Ladung romulanerLadung3 = new Ladung("Plasma-Waffe", 50);

        romulaner.addLadung(romulanerLadung1, romulanerLadung2, romulanerLadung3);

        Raumschiff vulkanier = new Raumschiff(
                0,
                80,
                80,
                50,
                100,
                5,
                "Ni'var"
        );

        Ladung vulkanierLadung1 = new Ladung("Forschungsonde", 35);
        Ladung vulkanierLadung2 = new Ladung("Photonentorpedo", 3);

        vulkanier.addLadung(vulkanierLadung1, vulkanierLadung2);
    }
}
