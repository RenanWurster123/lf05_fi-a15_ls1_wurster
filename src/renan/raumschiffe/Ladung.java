package renan.raumschiffe;

public class Ladung {
    private String bezeichnung;
    private int menge;


    public Ladung(){

    }

    public Ladung(String bezeichnung, int menge){
        setBezeichnung(bezeichnung);
        setMenge(menge);
    }


    public void setBezeichnung(String name){
          this.bezeichnung = name;
    }

    String getBezeichnung(){
        return this.bezeichnung;
    }

    public void setMenge(int menge){
        this.menge = menge;
    }

    int getMenge(){
        return this.menge;
    }

    @Override
    public String toString() {
        return "Bezeichnug: " + getBezeichnung() + ", Menge: " + getMenge();
    }
}
