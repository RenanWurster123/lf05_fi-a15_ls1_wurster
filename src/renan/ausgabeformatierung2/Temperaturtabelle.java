package renan.ausgabeformatierung2;

public class Temperaturtabelle {

    public static void main(String[] args) {

        String ab = "Fahrenheit";
        String bc = "Celsius";

        int a = 20;
        double b = 28.8889;
        int c = 10;
        double d = 23.3333;
        int e = 0;
        double f = 17.7778;
        int g = 20;
        double h = 6.6667;
        int i = 30;
        double j = 1.1111;

        System.out.printf("%-12s| %10s\n", ab,bc);
        System.out.println("------------------------");
        System.out.printf( "%-12d| %10.2f\n" , -a, -b);
        System.out.printf( "%-12d| %10.2f\n" , -c, -d);
        System.out.printf( "%-12d| %10.2f\n" , e, -f);
        System.out.printf( "%-12d| %10.2f\n" , g, -h);
        System.out.printf( "%-12d| %10.2f\n" , i, -j);
    }
}
