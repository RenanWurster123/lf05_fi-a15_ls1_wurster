package renan.schleifen;

import java.util.Scanner;

public class Treppe {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Wie hoch?");
        int hoehe = scanner.nextInt();
        System.out.println("Wie breit?");
        int breite = scanner.nextInt();


        int i = 0;
        while (i <= hoehe) {

            int j = 0;

            while (j < (breite * i)) {
                System.out.print("*");
                j++;
            }
            System.out.println();
            i++;
        }
    }
}