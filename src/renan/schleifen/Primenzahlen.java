package renan.schleifen;

import java.util.Scanner;

public class Primenzahlen {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Bis zur welcher Zahl soll nach Primzahlen gesucht werden?");
        int checkRadius = scanner.nextInt();

        for (int i = 2; i < checkRadius; i++) {
            boolean valid = true;

            for (int j = 2; j < i; j++) {
                if (i % j == 0) {
                    valid = false;
                    break;
                }
            }
            if (valid) System.out.println(i);
        }
        scanner.close();
    }
}
